package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import logic.Musiktitel;

/**
 * Speichert die Datenbankverbindung und führt die Abfragen aus
 * @author Tim Tenbusch
 */
public class DatabaseConnection {

	private String driver = "com.mysql.jdbc.Driver";
	private String url = "jdbc:mysql://localhost/axeoffire?";
	private String user = "root";
	private String password = "";


	/**
	 * trägt einen User ein 
	 * @param der Username des neuen Benutzers
	 * @exception neben regulären DB-Fehlern, wenn der Benutzer schon vorhanden ist    
	 */
	public boolean userEintragen(String username) {
		String sql = ("INSERT INTO TUser (P_Name) VALUES ('" + username + "');");
		try {
			// JDBC-Treiber laden
			Class.forName(driver);
			// Verbindung aufbauen
			Connection con = DriverManager.getConnection(url, user, password);
			PreparedStatement ps = con.prepareStatement(sql);
			//SQL-Statement ausführen
			ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Fehler mit SQL-Befehl: " + sql);
			return false;
		}
		return true;
	}
	
	/**
	 * prüft ob ein Benutzer bereits in der DB existiert
	 * @param username - der Bentzername
	 */
	public boolean userExists(String username) {
		String sql = "SELECT count(*) FROM TUser WHERE P_Name = '"+username+"';";
		try {
			// JDBC-Treiber laden
			Class.forName(driver);
			// Verbindung aufbauen
			Connection con;
			con = DriverManager.getConnection(url, user, password);
			// SQL-Anweisungen ausführen
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			// Ergebnis abfragen
			while (rs.next()) {
				if (rs.getInt(1) == 0)
					return false;
				else
					return true;
			}
			// Verbindung schlieäen
			con.close();
		} catch (Exception ex) { // Fehler abfangen
			ex.printStackTrace();// Fehlermeldung ausgeben
			return false;
		}
		return false;
	}

	/**
	 * trägt einen vollständig ausgefällten Vorschlag in die Datenbank ein
	 * @param m - Musiktitelobjekt
	 */
	public void musiktitelEintragen(Musiktitel m) {
		String sql = "INSERT INTO TMusiktitel (Band, Titelname, Genre) VALUES (?,?,?);";
		try {
			// JDBC-Treiber laden
			Class.forName(driver);
			// Verbindung aufbauen
			Connection con = DriverManager.getConnection(url, user, password);
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, m.getBandname());
			ps.setString(2, m.getTitelname());
			ps.setString(3, m.getGenre());
			ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Fehler mit SQL-Befehl: " + sql);
		}
	}

	/**
	 * gibt die Musiktitel als Liste aus
	 * @return Liste aller Musiktitel ohne Votes Sortiert nach Band, Titelname 
	 */
	public List<Musiktitel> getMusiktitel() {
		String sql = "SELECT * FROM TMusiktitel ORDER BY Band, Titelname;";
		List<Musiktitel> musikliste = new LinkedList<Musiktitel>();
		try {
			// JDBC-Treiber laden
			Class.forName(driver);
			// Verbindung aufbauen
			Connection con = DriverManager.getConnection(url, user, password);
			Statement s = con.createStatement();
			ResultSet rs = s.executeQuery(sql);
			while (rs.next())
				musikliste.add(new Musiktitel(rs.getString(3), rs.getString(2), rs.getString(4)));
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Fehler mit SQL-Befehl: " + sql);
		}
		return musikliste;
	}

	/**
	 * trägt einen Vote ein
	 */
	public void voteEintragen(String loginname, Musiktitel m) {
		String sql = "INSERT INTO TVotes (P_Vote_id, PF_Musik_id, PF_Name) VALUES (?,(SELECT MAX(P_Musik_id) FROM TMusiktitel WHERE Band = ? AND Titelname = ? AND Genre = ?),?);";
		try {
			// JDBC-Treiber laden
			Class.forName(driver);
			// Verbindung aufbauen
			Connection con = DriverManager.getConnection(url, user, password);
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, getNewVoteID(loginname));
			ps.setString(2, m.getBandname());
			ps.setString(3, m.getTitelname());
			ps.setString(4, m.getGenre());
			ps.setString(5, loginname);
			ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Fehler mit SQL-Befehl: " + sql);
		}
	}

	/**
	 * gibt zu einem Benutzernamen die nächste VoteID aus
	 * @param loginname
	 * @return neue VoteID
	 */
	private int getNewVoteID(String loginname){
		String sql = "SELECT MAX(P_Vote_id) FROM TVotes WHERE PF_Name = '" + loginname + "';";
		int maxVoteID = 0;
		try {
			// JDBC-Treiber laden
			Class.forName(driver);
			// Verbindung aufbauen
			Connection con = DriverManager.getConnection(url, user, password);
			Statement s = con.createStatement();
			ResultSet rs = s.executeQuery(sql);
			while(rs.next())
				maxVoteID = rs.getInt(1);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Fehler mit SQL-Befehl: " + sql);
		}
		return maxVoteID + 1; //neue VoteID erzeugen
	}
	
	/**
	 * gibt die Playlist aus
	 */
	public String[][] getPlaylist() {
		String sql = "SELECT COUNT(*) AS Anzahl, Titelname, Band, Genre " + "FROM TUser INNER JOIN TVotes ON P_name = PF_name " + "INNER JOIN TMusiktitel ON P_Musik_id = PF_Musik_id "
				+ "WHERE P_Vote_id >= ((SELECT MAX(P_Vote_id) FROM TVotes WHERE PF_Name = P_Name)-4) " + "GROUP BY Titelname, Band, Genre " + "ORDER BY Anzahl DESC, Band ASC;";
		List<String[]> ergebnisliste = new Vector<String[]>();
		try {
			// JDBC-Treiber laden
			Class.forName(driver);
			// Verbindung aufbauen
			Connection con = DriverManager.getConnection(url, user, password);
			Statement s = con.createStatement();
			ResultSet rs = s.executeQuery(sql);
			while (rs.next()) {
				String[] datensatz = new String[4];
				datensatz[0] = rs.getString(1);
				datensatz[1] = rs.getString(2);
				datensatz[2] = rs.getString(3);
				datensatz[3] = rs.getString(4);
				ergebnisliste.add(datensatz);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Fehler mit SQL-Befehl: " + sql);
		}
		String[][] playlist = new String[ergebnisliste.size()][4];
		int i = 0;
		for (String[] s : ergebnisliste)
			playlist[i++] = s;
		return playlist;
	}

}
