CREATE TABLE IF NOT EXISTS `tmusiktitel` (
  `P_Musik_id` int(11) NOT NULL AUTO_INCREMENT,
  `Titelname` varchar(40) NOT NULL,
  `Band` varchar(40) NOT NULL,
  `Genre` varchar(20) NOT NULL,
  PRIMARY KEY (`P_Musik_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tuser` (
  `P_Name` varchar(90) NOT NULL,
  PRIMARY KEY (`P_Name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tvotes` (
  `P_Vote_id` int(11) NOT NULL,
  `PF_name` varchar(90) DEFAULT NULL,
  `PF_Musik_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`P_Vote_id`, `PF_name`, `PF_Musik_id`),
  KEY `fk1` (`PF_name`),
  KEY `fk2` (`PF_Musik_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

ALTER TABLE `tvotes`
  ADD CONSTRAINT `fk1` FOREIGN KEY (`PF_name`) REFERENCES `tuser` (`P_Name`),
  ADD CONSTRAINT `fk2` FOREIGN KEY (`PF_Musik_id`) REFERENCES `tmusiktitel` (`P_Musik_id`);

SELECT COUNT(*) AS Anzahl, Titelname, Band, Genre 
FROM TUser INNER JOIN TVotes 
	ON P_name = F_name 
INNER JOIN TMusiktitel 
	ON P_MusikID = F_MusikID 
WHERE P_VoteID >= ((SELECT MAX(P_VoteID) 
					FROM TVotes 
					WHERE F_Name = P_Name)-4) 
GROUP BY Titelname, Band, Genre 
ORDER BY Anzahl DESC, Band ASC;

ALTER TABLE tuser
ADD COLUMN Hausname VARCHAR(30);
ALTER TABLE tuser
ADD COLUMN Geupdate BOOLEAN DEFAULT FALSE;