package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import logic.MusicvotingLogik;
import logic.Musiktitel;

public class MusikVotenGUI extends JFrame {

	private static final long serialVersionUID = 4510828557344012125L;
	private JPanel contentPane;
	private JTextField tfdLoginname;
	private JTable tableMusiktitel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MusikVotenGUI frame = new MusikVotenGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MusikVotenGUI() {
		setTitle("Musikvoting der Gruppe Relativ Gro�er Ginja Hobbit - Voting");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel pnlNorth = new JPanel();
		pnlNorth.setBackground(new Color(128, 0, 128));
		contentPane.add(pnlNorth, BorderLayout.NORTH);
		pnlNorth.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JLabel lblLoginname = new JLabel("Loginname:");
		lblLoginname.setForeground(Color.WHITE);
		lblLoginname.setHorizontalAlignment(SwingConstants.LEFT);
		lblLoginname.setFont(new Font("Comic Sans MS", Font.BOLD, 14));
		pnlNorth.add(lblLoginname);
		
		
		tfdLoginname = new JTextField();
		pnlNorth.add(tfdLoginname);
		tfdLoginname.setColumns(10);
		
		JButton btnVoten = new JButton("Voten");
		btnVoten.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(tfdLoginname.getText().equals(""))
					tfdLoginname.setBackground(Color.RED);
				else{
					tfdLoginname.setBackground(Color.WHITE);
					String loginname = tfdLoginname.getText();
					int index = tableMusiktitel.getSelectedRow();
					Musiktitel m = new Musiktitel((String)tableMusiktitel.getValueAt(index, 0), (String)tableMusiktitel.getValueAt(index, 1), (String)tableMusiktitel.getValueAt(index, 2));
					MusicvotingLogik mvl = new MusicvotingLogik();
					mvl.voteEintragen(loginname, m);
				}
			}
		});
		contentPane.add(btnVoten, BorderLayout.EAST);
		
		String[] spaltennamen = {"Bandname",
                "Titelname",
                "Genre"};
		String[][] data = (new MusicvotingLogik()).getMusiktitel();
		tableMusiktitel = new JTable(data, spaltennamen);
		tableMusiktitel.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
		
		JScrollPane scrollPane = new JScrollPane(tableMusiktitel);
		tableMusiktitel.setFillsViewportHeight(true);
		contentPane.add(scrollPane, BorderLayout.CENTER);
	}
}
